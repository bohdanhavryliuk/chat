import React from 'react';

import logo from '../../logo.svg';
import '../../styles.css';

function Preloader() {
  return (
    <div className="preloader">
        <img src={logo} alt="logo" />
    </div>
  //   <div className="preloader">
  //   <span className="sr-only">Loading...</span>
  // </div>
  );
}

export default Preloader;