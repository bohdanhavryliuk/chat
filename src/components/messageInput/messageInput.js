import React from "react";

function MessageInput({ messages, setMessages, editingMessage }) {

    let textInput = React.createRef();

    // document.addEventListener("keydown", setMessage);

    // function setMessage(e) {

    //     if(e.code === "Enter") {
    //         setMessages({
    //             isLoaded: true, items: [
    //                 { id: String(Math.random()) + String(Math.random()), userId: "me", user: "I", text: textInput.current.value, createAt: new Date().toLocaleString(), editedAt: "" }
    //                 , ...messages,]
    //         });
    //     }
    // }
    function setMessage() {
        if (textInput.current.value !== "") {
            setMessages({
                isLoaded: true, items: [
                    { id: String(Math.random()) + String(Math.random()), userId: "me", user: "I", text: textInput.current.value, createdAt: Date().substr(0,24), editedAt: "" }
                    , ...messages,]
            });    
            document.getElementsByClassName("message-input-text")[0].value = "";
        }
    }

    function editMessage(message) {
        if (textInput.current.value !== "") {
            
            messages.forEach(element => {
                if (element === message) {
                    element.text = textInput.current.value;
                }
            });

            setMessages({
                isLoaded: true, items: [...messages,]
            });

            document.getElementsByClassName("message-input-text")[0].value = "";
        }
    }

    return (
        <div className="message-input">
            <input className="message-input-text" type="text" ref={textInput}></input>
            <button className="message-input-button" onClick={setMessage}>Send</button>
        </div>
    );
}

export default MessageInput;