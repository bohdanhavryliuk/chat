import React, { useState } from "react";

function Message({message}) {

    let [like, setLike] = useState(false);

    function pressLike() {
        like ? setLike(false) : setLike(true);
    }

    return (
        <div className="message">
            <p className="message-text">{message.text}</p>
            <p className="message-time">{message.createdAt.substr(11,8)}</p>
            <p className="message-user-name">{message.user}</p>
            <img className="message-user-avatar" src={message.avatar}></img>
            {like ? (<button className="message-like" onClick={pressLike}>Like</button>) : (
                <button onClick={pressLike}>Like</button>
            )}
        </div>
    );
}

export default Message;