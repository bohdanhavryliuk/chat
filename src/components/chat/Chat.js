import '../../styles.css';
import React, { /* useMemo, */ useState } from 'react';
import Header from '../header/header';
import Preloader from '../preloader/preloader';
import MessageList from '../messageList/messageList';
import MessageInput from '../messageInput/messageInput';

function Chat({ url }) {
  //const [isDownloaded, loadState] = React.useState(false)
  const [state, setState] = useState({ isLoaded: false, items: [] })

  if (!state.isLoaded) {
    fetch(url)
      .then(resourse => resourse.json())
      .then(data => { setState({ isLoaded: true, items: data.reverse() }) });
  }

  //console.log(messages)
  // useMemo(() => {
  //   fetch(url)
  //     .then(resourse => resourse.json())
  //     .then(data => { setState({ isLoaded: true, items: data }); console.log('да') });
  // }, [url]);

  function deleteMessage(message) {
    state.items = state.items.filter(item => item !== message)
    setState({ isLoaded: true, items: state.items });
  }

  return (
    <div className='chat'>
      {state.isLoaded ? (
        <>
          <Header messages={state.items}/>
          <MessageList messages={state.items} deletedMessage={deleteMessage}/>
          <MessageInput messages={state.items} setMessages={setState} />
        </>
      ) : (
        <Preloader />
      )}
    </div>


  );
}

export default Chat;
