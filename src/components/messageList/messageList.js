import Message from "../message/message";
import OwnMessage from "../ownMessage/ownMessage";


function MessageList({ messages, deletedMessage, editMessage }) {

    // useEffect(()=>{
    //     console.log("messages has increased");
    // },[messages.length]);

    return (
        <div className="message-list">
            {messages.map(message => message.userId === "me" ?
                (
                    <OwnMessage key={message.id} message={message} deleteMessage={() => deletedMessage(message)} /* editMessage={() => editMessage(message)}  *//>
                ) : (
                    <Message key={message.id} message={message} />
                )
            )}
        </div>
    );
}

export default MessageList;