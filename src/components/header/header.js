import '../../styles.css';
import React from 'react';

function Header({messages}) {
    
    return (
        <div className='header'>
            <div className='header-title'>Chat</div>
            <div className='header-users-count'>Учасників: 12</div>
            <div className='header-messages-count'>Повідомлень: {messages.length}</div>
            <div className='header-last-message-date'>{messages[0].createdAt}</div>
        </div>
    );
}

export default Header;