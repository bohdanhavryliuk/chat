
function OwnMessage({message, deleteMessage, editMessage}) {
    return (
        <div className="own-message">
            <p className="message-time">{message.createdAt.substr(16,8)}</p>
            <p className="message-text">{message.text}</p>
            <button className="message-edit" onClick={editMessage}>Edit</button>
            <button className="message-delete" onClick={deleteMessage}>Delete</button>
 


        </div>
    );
}

export default OwnMessage;